# Copyright 2020-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYTHON_COMPAT=( python3_{10..12} )

inherit xdg cmake flag-o-matic python-any-r1 toolchain-funcs optfeature

DESCRIPTION="An experimental Telegram Desktop fork"
HOMEPAGE="https://kotatogram.github.io/"

inherit git-r3

EGIT_REPO_URI="https://github.com/kotatogram/kotatogram-desktop.git"
EGIT_SUBMODULES=(
    '*'
    '-Telegram/ThirdParty/Catch'
    '-Telegram/ThirdParty/libdbusmenu-qt'
    '-Telegram/ThirdParty/lz4'
)

if [[ ${PV} == 9999 ]]; then
	EGIT_BRANCH="patches-track-4.14"
	KEYWORDS=""
else
	EGIT_COMMIT="k${PV}"
	KEYWORDS="~amd64 ~x86"
fi

LICENSE="BSD GPL-3-with-openssl-exception LGPL-2+"
SLOT="0"
IUSE="custom-api-id dbus enchant +fonts screencast qt6 qt6-imageformats wayland webkit +X debug test"

REQUIRED_USE="
	qt6-imageformats? ( qt6 )
"

KIMAGEFORMATS_RDEPEND="
	media-libs/libavif:=
	media-libs/libheif:=
	>=media-libs/libjxl-0.8.0:=
"
CDEPEND="
	!net-im/kotatogram-desktop-bin
	app-arch/lz4:=
	dev-cpp/abseil-cpp:=
	>=dev-cpp/glibmm-2.77:2.68
	dev-libs/glib:2
	dev-libs/libdispatch
	dev-libs/openssl:=
	dev-libs/protobuf
	dev-libs/xxhash
	media-libs/libjpeg-turbo:=
	~media-libs/libtgvoip-2.4.4_p20221208
	media-libs/openal
	media-libs/opus
	media-libs/rnnoise
	~media-libs/tg_owt-0_pre20230921:=[screencast=,X=]
	media-video/ffmpeg:=[opus,vpx]
	sys-libs/zlib:=[minizip]
	virtual/opengl
	!enchant? ( >=app-text/hunspell-1.7:= )
	enchant? ( app-text/enchant:= )
	!qt6? (
		>=dev-qt/qtcore-5.15:5=
		>=dev-qt/qtgui-5.15:5=[dbus?,jpeg,png,wayland?,X?]
		>=dev-qt/qtimageformats-5.15:5
		>=dev-qt/qtnetwork-5.15:5[ssl]
		>=dev-qt/qtsvg-5.15:5
		>=dev-qt/qtwidgets-5.15:5[png,X?]
		kde-frameworks/kcoreaddons:5
		webkit? (
			>=dev-qt/qtdeclarative-5.15:5
			>=dev-qt/qtwayland-5.15:5
		)
	)
	qt6? (
		>=dev-qt/qtbase-6.5:6=[dbus?,gui,network,opengl,wayland?,widgets,X?]
		>=dev-qt/qtimageformats-6.5:6
		>=dev-qt/qtsvg-6.5:6
		webkit? (
			>=dev-qt/qtdeclarative-6.5:6
			>=dev-qt/qtwayland-6.5:6[compositor]
		)
		qt6-imageformats? (
			>=dev-qt/qtimageformats-6.5:6=
			${KIMAGEFORMATS_RDEPEND}
		)
	)
	X? (
		x11-libs/libxcb:=
		x11-libs/xcb-util-keysyms
	)
	test? ( dev-cpp/catch )
"
RDEPEND="${CDEPEND}
	webkit? ( net-libs/webkit-gtk:4.1 net-libs/webkit-gtk:6 )
"
DEPEND="${CDEPEND}
	>=dev-cpp/cppgir-0_p20240110
	>=dev-cpp/ms-gsl-4
	dev-cpp/expected-lite
	dev-cpp/range-v3
"
BDEPEND="
	${PYTHON_DEPS}
	>=dev-build/cmake-3.16
	>=dev-cpp/cppgir-0_p20230926
	dev-util/gdbus-codegen
	virtual/pkgconfig
	wayland? ( dev-util/wayland-scanner )
"
# dev-libs/jemalloc:=[-lazy-lock] -> https://bugs.gentoo.org/803233

#if [[ ${PV} == 9999 ]]; then
PATCHES=(
	"${FILESDIR}/tdesktop-4.10.0-system-cppgir.patch"
	"${FILESDIR}/tdesktop-4.10.5-qt_compare.patch"
	"${FILESDIR}/kotatogram-4.14-cppgir.patch"
	"${FILESDIR}/antifeatures/0001-Disable-sponsored-messages.patch"
	"${FILESDIR}/antifeatures/0002-Disable-saving-restrictions.patch"
	"${FILESDIR}/antifeatures/0003-Disable-invite-peeking-restrictions.patch"
	"${FILESDIR}/antifeatures/0005-Option-to-disable-stories.patch"
)
#else
#	PATCHES=(
#	)
#fi

pkg_pretend() {
	if has ccache ${FEATURES}; then
		ewarn "ccache does not work with ${PN} out of the box"
		ewarn "due to usage of precompiled headers"
		ewarn "check bug https://bugs.gentoo.org/715114 for more info"
		ewarn
	fi

	if use custom-api-id; then
		[[ -n "${MY_TDESKTOP_API_ID}" ]] && \
		[[ -n "${MY_TDESKTOP_API_HASH}" ]] && (
			einfo "Will be used custom 'api_id' and 'api_hash':"
			einfo "MY_TDESKTOP_API_ID=${MY_TDESKTOP_API_ID}"
			einfo "MY_TDESKTOP_API_HASH=${MY_TDESKTOP_API_HASH//[!\*]/*}"
		) || (
			eerror "It seems you did not set one or both of"
			eerror "MY_TDESKTOP_API_ID and MY_TDESKTOP_API_HASH variables,"
			eerror "which are required for custom-api-id USE-flag."
			eerror "You can set them either in your env or bashrc."
			die
		)

		echo ${MY_TDESKTOP_API_ID} | grep -q "^[0-9]\+$" || (
			eerror "Please check your MY_TDESKTOP_API_ID variable"
			eerror "It should consist of decimal numbers only"
			die
		)

		echo ${MY_TDESKTOP_API_HASH} | grep -q "^[0-9A-Fa-f]\{32\}$" || (
			eerror "Please check your MY_TDESKTOP_API_HASH variable"
			eerror "It should consist of 32 hex numbers only"
			die
		)
	fi

	#if tc-is-gcc && [[ $(gcc-major-version) -lt 7 ]]; then
	#	die "At least gcc 7.0 is required"
	#fi
}

git_unpack() {
	git-r3_src_unpack

	unset EGIT_BRANCH
	unset EGIT_SUBMODULES
	if [[ ${PV} != 9999 ]]; then
		unset EGIT_COMMIT
	fi

	EGIT_COMMIT_DATE=$(GIT_DIR="${S}/.git" git show -s --format=%ct || die)

	EGIT_REPO_URI="https://code.qt.io/qt/qtbase.git"
	EGIT_CHECKOUT_DIR="${WORKDIR}"/Libraries/qtbase

	git-r3_src_unpack

#	EGIT_REPO_URI="https://github.com/ericniebler/range-v3.git"
#	EGIT_CHECKOUT_DIR="${WORKDIR}"/Libraries/range-v3

#	git-r3_src_unpack
}

src_unpack() {
	default

	git_unpack
	return

	mkdir Libraries || die
#	mv range-v3-${RANGE_V3_VER} Libraries/range-v3 || die
	mv qtbase-everywhere-src-${QTBASE_VER} Libraries/qtbase || die
}

qt_prepare() {
	local qt_src="${WORKDIR}"/Libraries/qtbase/src
	local qt_fun="${S}"/Telegram/SourceFiles/qt_functions.cpp

	echo "#include <QtGui/private/qtextengine_p.h>" > "${qt_fun}"

#	if use gtk3; then
	sed '/^QStringList.*qt_make_filter_list.*QString/,/^\}/!d' \
		"${qt_src}"/widgets/dialogs/qfiledialog.cpp >> "${qt_fun}"
#	fi

	sed '/^QTextItemInt::QTextItemInt.*QGlyphLayout/,/^\}/!d' \
		"${qt_src}"/gui/text/qtextengine.cpp >> "${qt_fun}"

	sed '/^void.*QTextItemInt::initWithScriptItem.*QScriptItem/,/^\}/!d' \
		"${qt_src}"/gui/text/qtextengine.cpp >> "${qt_fun}"

}

src_prepare() {
	# Bundle kde-frameworks/kimageformats for qt6, since it's impossible to
	#   build in gentoo right now.
	if use qt6-imageformats; then
		sed -e 's/DESKTOP_APP_USE_PACKAGED_LAZY/TRUE/' -i \
			cmake/external/kimageformats/CMakeLists.txt || die
		printf '%s\n' \
			'Q_IMPORT_PLUGIN(QAVIFPlugin)' \
			'Q_IMPORT_PLUGIN(HEIFPlugin)' \
			'Q_IMPORT_PLUGIN(QJpegXLPlugin)' \
			>> cmake/external/qt/qt_static_plugins/qt_static_plugins.cpp || die
	fi
	# kde-frameworks/kcoreaddons is bundled when using qt6.

	# Happily fail if libraries aren't found...
	find -type f \( -name 'CMakeLists.txt' -o -name '*.cmake' \) \
		\! -path './Telegram/lib_webview/CMakeLists.txt' \
		\! -path './cmake/external/expected/CMakeLists.txt' \
		\! -path './cmake/external/kcoreaddons/CMakeLists.txt' \
		\! -path './cmake/external/qt/package.cmake' \
		-print0 | xargs -0 sed -i \
		-e '/pkg_check_modules(/s/[^ ]*)/REQUIRED &/' \
		-e '/find_package(/s/)/ REQUIRED)/' || die
	# Make sure to check the excluded files for new
	# CMAKE_DISABLE_FIND_PACKAGE entries.

	# Control QtDBus dependency from here, to avoid messing with QtGui.
	if ! use dbus; then
		sed -e '/find_package(Qt[^ ]* OPTIONAL_COMPONENTS/s/DBus *//' \
			-i cmake/external/qt/package.cmake || die
	fi

	cmake_src_prepare
}

src_configure() {
	# Having user paths sneak into the build environment through the
	# XDG_DATA_DIRS variable causes all sorts of weirdness with cppgir:
	# - bug 909038: can't read from flatpak directories (fixed upstream)
	# - bug 920819: system-wide directories ignored when variable is set
	export XDG_DATA_DIRS="${EPREFIX}/usr/share"

	# Evil flag (bug #919201)
	filter-flags -fno-delete-null-pointer-checks

	# The ABI of media-libs/tg_owt breaks if the -DNDEBUG flag doesn't keep
	# the same state across both projects.
	# See https://bugs.gentoo.org/866055
	append-cppflags '-DNDEBUG'

	local qt=$(usex qt6 6 5)
	local mycmakeargs=(
		-DDESKTOP_APP_USE_PACKAGED=ON
		-DDESKTOP_APP_DISABLE_CRASH_REPORTS=ON
		-DTDESKTOP_LAUNCHER_BASENAME="${PN}"

		-DQT_VERSION_MAJOR=${qt}

		# Override new cmake.eclass defaults (https://bugs.gentoo.org/921939)
		# Upstream never tests this any other way
		-DCMAKE_DISABLE_PRECOMPILE_HEADERS=OFF

		# Control automagic dependencies on certain packages
		## Header-only lib, some git version.
		-DCMAKE_DISABLE_FIND_PACKAGE_tl-expected=ON
		-DCMAKE_DISABLE_FIND_PACKAGE_Qt${qt}Quick=$(usex !webkit)
		-DCMAKE_DISABLE_FIND_PACKAGE_Qt${qt}QuickWidgets=$(usex !webkit)
		-DCMAKE_DISABLE_FIND_PACKAGE_Qt${qt}WaylandClient=$(usex !wayland)
		## Only used in Telegram/lib_webview/CMakeLists.txt
		-DCMAKE_DISABLE_FIND_PACKAGE_Qt${qt}WaylandCompositor=$(usex !webkit)
		## KF6CoreAddons is currently unavailable in ::gentoo
		-DCMAKE_DISABLE_FIND_PACKAGE_KF${qt}CoreAddons=$(usex qt6)

		-DDESKTOP_APP_DISABLE_X11_INTEGRATION=$(usex !X)
		-DDESKTOP_APP_DISABLE_WAYLAND_INTEGRATION=$(usex !wayland)
		## Enables enchant and disables hunspell
		-DDESKTOP_APP_USE_ENCHANT=$(usex enchant)
		## Use system fonts instead of bundled ones
		-DDESKTOP_APP_USE_PACKAGED_FONTS=$(usex !fonts)
	)

	if use custom-api-id; then
		mycmakeargs+=(
			-DTDESKTOP_API_ID="${MY_TDESKTOP_API_ID}"
			-DTDESKTOP_API_HASH="${MY_TDESKTOP_API_HASH}"
		)
	else
		# https://github.com/telegramdesktop/tdesktop/blob/dev/snap/snapcraft.yaml
		# Building with snapcraft API credentials by default
		# Custom API credentials can be obtained here:
		# https://github.com/telegramdesktop/tdesktop/blob/dev/docs/api_credentials.md
		# After getting credentials you can export variables:
		#  export MY_TDESKTOP_API_ID="17349""
		#  export MY_TDESKTOP_API_HASH="344583e45741c457fe1862106095a5eb"
		# and restart the build"
		# you can set above variables (without export) in /etc/portage/env/net-im/kotatogram-desktop
		# portage will use custom variable every build automatically
		mycmakeargs+=(
			-DTDESKTOP_API_ID="611335"
			-DTDESKTOP_API_HASH="d524b414d21f4d37f08684c1df41ac9c"
		)
	fi

	cmake_src_configure
}

pkg_postinst() {
	xdg_pkg_postinst
	if ! use X && ! use screencast; then
		ewarn "both the 'X' and 'screencast' USE flags are disabled, screen sharing won't work!"
		ewarn
	fi
	if use wayland && ! use qt6; then
		ewarn "Wayland-specific integrations have been deprecated with Qt5."
		ewarn "The app will continue to function under wayland, but some"
		ewarn "functionality may be reduced."
		ewarn "These integrations are only supported when built with Qt6."
		ewarn
	fi
	if use qt6 && ! use qt6-imageformats; then
		elog "Enable USE=qt6-imageformats for AVIF, HEIF and JpegXL support"
		elog
	fi
	optfeature_header
	if ! use qt6; then
		optfeature "AVIF, HEIF and JpegXL image support" kde-frameworks/kimageformats[avif,heif,jpegxl]
	fi
}
